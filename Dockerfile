FROM mbbteam/mbb_workflows_base:latest as alltools

RUN wget https://github.com/OpenGene/fastp/archive/v0.20.0.tar.gz \
 && tar -xvzf v0.20.0.tar.gz \
 && cd fastp-0.20.0 \
 && make \
 && mv fastp /opt/biotools/bin/fastp \
 && cd .. \
 && rm -r fastp-0.20.0 v0.20.0.tar.gz 

RUN cd /opt/biotools/bin \
 && wget -O jellyfish https://github.com/gmarcais/Jellyfish/releases/download/v2.3.0/jellyfish-linux \
 && chmod +x /opt/biotools/bin/jellyfish

RUN cd /opt/biotools \
 && wget -O genomescope.tar.gz https://github.com/schatzlab/genomescope/archive/v1.0.0.tar.gz \
 && tar -xvzf genomescope.tar.gz \
 && mv genomescope-1.0.0/genomescope.R . \
 && rm -r genomescope-1.0.0 genomescope.tar.gz

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

