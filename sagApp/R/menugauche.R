MenuGauche = sidebarMenu(id="sidebarmenu",

	menuItem("Global parameters", tabName="global_params", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("Preprocessing", tabName="preprocessing", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("K-mer counting", tabName="kmer_counting", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("K-mer histogram", tabName="kmer_histogram", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("K-mer analysis", tabName="kmer_analysis", icon=icon("pencil", lib="font-awesome"), newtab=FALSE),

	menuItem("Draw workflow graph", tabName="RULEGRAPH", icon=icon("gear", lib="font-awesome"), newtab=FALSE),

	tags$br(),

	downloadButton("DownloadParams", "Download config file", class="btn btn-light", style="color:black;margin: 6px 5px 6px 15px;"),

	tags$br(),

	tags$br(),

	numericInput("cores", label = "Threads available", min = 1, max = 24, step = 1, width =  "auto", value = 16),
selectInput("force_from", label = "Start again from a step : ", selected = "none", choices = list('No'='none','Preprocessing'='preprocessing','K-mer counting'='kmer_counting','K-mer histogram'='kmer_histogram','K-mer analysis'='kmer_analysis')),	tags$br(),
	actionButton("RunPipeline", "Run pipeline",  icon("play"), class="btn btn-info"),

	actionButton("StopPipeline", "Stop pipeline",  icon("stop"), class="btn btn-secondary"),
	tags$br(),
	tags$br(),
	menuItem("Running Workflow output", tabName="run_out", icon=icon("terminal", lib="font-awesome"), newtab=FALSE),
	menuItem("Final report", tabName="Report", icon=icon("file", lib="font-awesome"), newtab=FALSE),

	tags$br(),
	actionButton("close_session", "Close session",  icon("times"), class="btn btn-primary"),
	tags$br(),tags$br(),

	menuItem("Powered by mbb", href="http://mbb.univ-montp2.fr/MBB/index.php", newtab=TRUE, icon=icon("book", lib="font-awesome"), selected=NULL)

)


