#@author jimmy.lopez@univ-montp2.fr



library(shiny)
library(shinydashboard)
library(shinyjs)
library(yaml)
library(stringr)
library(shinyFiles)
library(tools)
library(DT)


source("./R/chooser.R", local=T)

source("./pages/pages_def_global_params.R", local=T)
source("./pages/pages_def_preprocessing.R", local=T)
source("./pages/pages_def_kmer_counting.R", local=T)
source("./pages/pages_def_kmer_histogram.R", local=T)
source("./pages/pages_def_kmer_analysis.R", local=T)
tabRULEGRAPH = fluidPage(box(title = 'Rule Graph :', width = 12, status = 'primary', collapsible = TRUE, solidHeader = TRUE, uiOutput('RULEGRAPH_svg'),actionButton('refresh_rg', 'Refresh',  icon('sync'), class='btn btn-info')))
tabReport = fluidPage(box(title = 'Report :', width = 12, status = 'primary', collapsible = TRUE, solidHeader = TRUE, uiOutput('report_html')))
tabRUN = fluidPage(box(title = 'Run :', width = 12 , status = 'primary', collapsible = TRUE, solidHeader = TRUE, uiOutput('run_out',style = 'overflow-y: scroll; height: 600px')),actionButton("unlock", "Unlock the directory in case of previous failure"), checkboxInput("rerun_incomplete", "Check in case of incomplete jobs to rerun", value = FALSE, width = NULL))
source("./R/menugauche.R", local=T)



style <- tags$style(HTML(readLines("www/added_styles.css")))

UI <- dashboardPage(

    skin="blue",

    dashboardHeader(title="Genome_Profile", titleWidth=230),

    dashboardSidebar(width=230, MenuGauche),

    dashboardBody(

        shinyjs::useShinyjs(),

                 tags$head(tags$link(rel="stylesheet", type="text/css", href="bootstrap.min.readable.css")),

tags$head(style),

	tabItems(

	tabItem(tabName = "global_params",         tabglobal_params),

	tabItem(tabName = "preprocessing",         tabpreprocessing),

	tabItem(tabName = "kmer_counting",         tabkmer_counting),

	tabItem(tabName = "kmer_histogram",         tabkmer_histogram),

	tabItem(tabName = "kmer_analysis",         tabkmer_analysis)

	,tabItem(tabName = "RULEGRAPH",         tabRULEGRAPH)
	,tabItem(tabName = "Report",         tabReport)
	,tabItem(tabName = "run_out",         tabRUN)
     )

)

)

reload = function(dossierAnalyse,session,output){
	# if params exists reload them
	if (file.exists(paste0(dossierAnalyse,"/params.yml"))){
		params = read_yaml(paste0(dossierAnalyse,"/params.yml"), handlers=list("float#fix"=function(x){ format(x,scientific=F)}))
		for (param in names(params$params_info)){
			if (params$params_info[[param]]$type == "text" || params$params_info[[param]]$type == "input_dir" || params$params_info[[param]]$type == "output_dir"){
				updateTextInput(session, param, value = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "textArea"){
				updateTextAreaInput(session, paste0(param,"_server"), value = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "input_file" && params[["params"]][[paste0(param,"_select")]] == "server"){
				updateTextInput(session, paste0(param,"_server"), value = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "numeric"){
				updateNumericInput(session, param, value = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "radio"){
				updateRadioButtons(session, param, selected = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "select"){
				updateSelectInput(session, param, selected = params[["params"]][[param]])
			}
			if (params$params_info[[param]]$type == "checkbox"){
				updateCheckboxInput(session, param, value = params[["params"]][[param]])
			}
		}
		for (step in params$steps){
			updateSelectInput(session, paste0("select",step$name), selected = params[["params"]][[step$name]])
		}
	}
	# if rulegraph show it
	rulegraph = list.files(path=dossierAnalyse,pattern="rulegraph[[:digit:]]+.svg")
	if (length(rulegraph) == 1){
		addResourcePath("results", dossierAnalyse)
		output$RULEGRAPH_svg = renderUI(tagList(img(src = paste0("results/",rulegraph) ,alt = "Rulegraph of Snakemake jobs",style="max-width: 100%;height: auto;display: block;margin: auto")))
	}
	else{
			output$RULEGRAPH_svg = renderUI(tagList(h3("No rule graph found, press the rule graph button to generate one.")))
	}
	# if report show it
	if (file.exists(paste0(dossierAnalyse,"/multiqc_report.html"))){
		addResourcePath("results", dossierAnalyse)
		output$report_html = renderUI(tags$iframe(src=paste0("results/multiqc_report.html"),width="100%", height="900px"))
	}
	else{
			output$report_html = renderUI(tags$h3("No report found, run the pipeline to produce a report"))
	}
}


server <- function( input, output, session) {

	rv <- reactiveValues(textstream = c(""), running = FALSE, timer = reactiveTimer(1000))

observeEvent(input$results_dir,{
		if (dir.exists(input$results_dir)){
			reload(input$results_dir,session,output)
			shinyjs::disable("results_dir")
			if (file.exists(paste0(input$results_dir,"/logs/workflow.running"))){
				rv$running = TRUE
			}
		}
		else{
			output$RULEGRAPH_svg = renderUI(tagList(h3("No rule graph found, press the rule graph button to generate one.")))
			output$report_html = renderUI(tags$h3("No report found, run the pipeline to produce a report"))
		}
})
	observe({
		rv$timer()
		if (isolate(rv$running)){
			if (file.exists(paste0(input$results_dir,"/logs/runlog.txt"))){
				rv$textstream <- paste(readLines(paste0(input$results_dir,"/logs/runlog.txt"),warn=F), collapse = "<br/>")
			}
			else{
				if (!dir.exists(paste0(input$results_dir,"/logs"))){
					dir.create(paste0(input$results_dir,"/logs"))
				}
				file.create(paste0(input$results_dir,"/logs/runlog.txt"))
			}
			if (file.exists(paste0(input$results_dir,"/logs/workflow_end.ok"))){
				isolate({rv$running = F})
				toggle_inputs(reactiveValuesToList(input),T,F)
				addResourcePath("results", input$results_dir)
				outUI = tags$iframe(src=paste0("results/multiqc_report.html"),width="100%", height="900px")
				output$report_html = renderUI(outUI)
				file.remove(paste0(input$results_dir,"/logs/workflow_end.ok"))
				file.remove(paste0(input$results_dir,"/logs/workflow.running"))
			}
			else if (file.exists(paste0(input$results_dir,"/logs/workflow_end.error"))){
				isolate({rv$running = F})
				toggle_inputs(reactiveValuesToList(input),T,F)
				output$report_html = renderUI(HTML(paste(readLines(paste0(input$results_dir,"/logs/workflow_end.error"),warn=F), collapse = "<br/>")))
				file.remove(paste0(input$results_dir,"/logs/workflow_end.error"))
				file.remove(paste0(input$results_dir,"/logs/workflow.running"))
			}
		}
	})
	output$run_out <- renderUI({
		HTML(rv$textstream)
	})

toggle_inputs <- function(input_list,enable_inputs=T,only_buttons=FALSE)
	{
		# Subset if only_buttons is TRUE.
		if(only_buttons){
			buttons <- which(sapply(input_list,function(x) {any(grepl("Button",attr(x,"class")))}))
			input_list = input_list[buttons]
		}

		# Toggle elements
		for(x in setdiff(names(input_list),"results_dir")){
			if(enable_inputs){
				shinyjs::enable(x)} else {
					shinyjs::disable(x) }
		}
	shinyjs::enable("unlock")
	shinyjs::enable("StopPipeline")
	shinyjs::enable("close_session")
	}
	observeEvent(input$SeOrPe,{
		input_list <- reactiveValuesToList(input)
		SE <- which(sapply(names(input_list),function(x) {any(grepl("_SE$",x))}))
		PE <- which(sapply(names(input_list),function(x) {any(grepl("_PE$",x))}))
		for (element in SE){
			if (input$SeOrPe == "PE")
				shinyjs::hide(names(input_list)[element])
			else{
				shinyjs::show(names(input_list)[element])
			}
		}
		for (element in PE){
		if (input$SeOrPe == "SE")
				shinyjs::hide(names(input_list)[element])
			else{
				shinyjs::show(names(input_list)[element])
			}
		}
	})
observeEvent(input$StopPipeline,{
		system("pkill -f snakemake")
		if (file.exists(paste0(input$results_dir,"/logs/workflow.running"))){
			file.remove(paste0(input$results_dir,"/logs/workflow.running"))
		}
	})
observeEvent(input$close_session,{
		session$close();
	})
observeEvent(input$unlock,{
		system2("python3",paste0("-u -m snakemake -s /workflow/Snakefile --configfile ", paste0(input$results_dir,"/params.yml") ,	" --forcerun all -d ", input$results_dir ,	" --cores ", input$cores, " all --unlock"),wait = TRUE, stdout = paste0(input$results_dir,"/logs/runlog.txt"), stderr = paste0(input$results_dir,"/logs/runlog.txt"));
		if (file.exists(paste0(input$results_dir,"/logs/workflow.running"))){
			file.remove(paste0(input$results_dir,"/logs/workflow.running"))
		}
		input_list <- reactiveValuesToList(input)
		toggle_inputs(input_list,T,F)
	})
output$DownloadParams <- downloadHandler(
	filename = function() {
		paste0("params", Sys.Date(), ".yaml", sep="")
	},
	content = function(file) {
		save_params(file)
	})
source("./server/opt_global.R", local=T)


}



shinyApp(ui = UI, server = server)
