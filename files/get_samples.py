#!/usr/bin/env python3
# This script will take a directory and a parameter to tell if the reads are paired end or single end and return the sample list and the suffix
# Needs 2 arguments: reads_directory, SeOrPe
# SeOrPe is SE for single end reads and PE for paired end reads
# Usage: ./get_samples.py reads_directory SeOrPe
import os
import re
import csv
import sys

def sample_list(dir, SeOrPe):
    samples = list()
    suffixes = list()
    files = os.listdir(dir)
    if SeOrPe == "PE":
        regex = re.compile(r"^(.+?)(_R1|_R2)(.+)")
    else:
        regex = re.compile(r"^(.+?)(\..*)")
    for file in files:
        res = re.match(regex, file)
        if res:
            if res.group(1) not in samples:
                samples.append(res.group(1))
                if SeOrPe == "PE":
                    suffixes.append(res.group(3))
                else:
                    suffixes.append(res.group(2))
    
    if (len(set(suffixes)) == 1 ):
        return {'samples': sorted(samples), 'suffix': list(set(suffixes))[0]}
    else:
        exit("Files have different suffixes:" + ','.join(suffixes))

def main():
    if len(sys.argv) == 3:
        print(sample_list(sys.argv[1],sys.argv[2]))
    else:
        exit("""Needs 2 arguments: reads_directory, SeOrPe
Usage: ./get_samples.py reads_directory SeOrPe""")

if __name__ == "__main__":
    # execute only if run as a script
    main()
